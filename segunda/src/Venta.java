public class Venta {
    private Producto producto;
    private Integer cantidad;
    private Float monto;
    private Float igv;

    public void imprimir(){
        System.out.println("Producto: "+
                producto.getMarca()+" "+
                producto.getModelo()+" "+
                producto.getColor());
        System.out.println("Cantidad: "+getCantidad());
        System.out.println("IGV: "+getIgv());
        System.out.println("Monto Total: "+getMonto());
        System.out.println("-----------------");
    }


    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Float getMonto() {
        return monto;
    }

    public void setMonto(Float monto) {
        this.monto = monto;
    }

    public Float getIgv() {
        return igv;
    }

    public void setIgv(Float igv) {
        this.igv = igv;
    }
}
