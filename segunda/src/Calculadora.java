import java.util.Scanner;

public class Calculadora {
    private Double primero;
    private Double segundo;
    private Double resultado;
    public void sumar(){
        resultado = primero+segundo;
    }
    public void restar(){
        resultado = primero-segundo;
    }
    public void multiplicar(){
        resultado = primero*segundo;
    }
    public void dividir(){
        resultado = primero/segundo;
    }

    public static void main(String[] args) {
        Calculadora a = new Calculadora();
        Integer operacion,x;
        Scanner entrada = new Scanner(System.in);
        do{
            System.out.println("CALCULADORA");
            System.out.println("Digite el numero de la operacion que desea realizar");
            System.out.println("0: Salir");
            System.out.println("1: Sumar");
            System.out.println("2: Restar");
            System.out.println("3: Multiplicar");
            System.out.println("4: Dividir");
            operacion = entrada.nextInt();
            switch (operacion){
                case 0: System.exit(0);
                case 1:
                    System.out.println("Digite los numeros");
                    a.primero = entrada.nextDouble();
                    a.segundo = entrada.nextDouble();;
                    a.sumar();
                    System.out.println(a.resultado);
                    break;
                case 2:
                    System.out.println("Digite los numeros");
                    a.primero = entrada.nextDouble();;
                    a.segundo = entrada.nextDouble();;
                    a.restar();
                    System.out.println(a.resultado);
                    break;
                case 3:
                    System.out.println("Digite los numeros");
                    a.primero = entrada.nextDouble();;
                    a.segundo = entrada.nextDouble();;
                    a.multiplicar();
                    System.out.println(a.resultado);
                    break;
                case 4:
                    System.out.println("Digite los numeros");
                    a.primero = entrada.nextDouble();;
                    a.segundo = entrada.nextDouble();;
                    a.dividir();
                    System.out.println(a.resultado);
                    break;
            }
            System.out.println("Si desea realizar otra operacion digite 1");
            x = entrada.nextInt();
        }while (x==1);
    }
}
