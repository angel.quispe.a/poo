public class Transaccion {
    public static void main(String[] args) {
        Producto producto = new Producto();
        producto.setColor("verde");
        producto.setMarca("Pilot");
        producto.setModelo("Board Master");
        producto.setPrecio(10.5f);
        producto.setStock(15);
        for (int i = 0; i < 10 && producto.getStock()>0; i++){
            int cantidad = 1 + (int) (Math.random() * 10);
            vender(producto,cantidad);
        }
    }
    public static void vender(Producto producto, int cantidad){
        Venta venta = new Venta();
        venta.setCantidad(cantidad);
        venta.setProducto(producto);
        venta.setIgv(0.18f
                * venta.getProducto().getPrecio()
                * venta.getCantidad());
        venta.setMonto(venta.getIgv()
                + venta.getProducto().getPrecio()
                * venta.getCantidad());
        venta.imprimir();
        producto.setStock(producto.getStock() - venta.getCantidad());
    }
}
