package pe.edu.uni.fiis.polimorfismo;

public class Epson extends Impresora implements Eficiente,Renovable{
    public String imprimir(){
        return "epson";
    }

    public String mensaje() {
        System.out.println("mensaje epson");
        return "mensaje epson";
    }

    public String mensaje(String dato) {
        System.out.println("mensaje "+dato);
        return "mensaje epson";
    }

    public  Integer potenciar(){
        return 19;
    }

    public String reciclar() {
        return "Renovación";
    }
}
